import Vue from 'vue'
import Router from 'vue-router'
import Vuex from 'vuex'
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// services
import storeService from "./services/store/StoreService";
// components
import Header from './components/Header.vue'
import Image from './components/Image'
import Menu from './components/menu/Menu.vue'
import Breadcrum from './components/menu/Breadcrum.vue'
import Page from './components/Page.vue'
import Column from './components/Column.vue'
import Table from './components/Table.vue'
import Line from './components/Line.vue'
import TableHead from './components/TableHead.vue'
import TableBody from './components/TableBody.vue'
import SearchBar from './components/SearchBar.vue'
// pages
import App from './App.vue'
import ModulePage from './pages/ModulePage.vue'
import ModuleRole from './pages/ModuleRole.vue'
import ModuleUser from './pages/ModuleUser.vue'
import PageCreate from './pages/PageCreate.vue'
import RoleCreate from './pages/RoleCreate.vue'
import PageUpdate from './pages/PageUpdate.vue'
import RoleUpdate from './pages/RoleUpdate.vue'
import Home from './pages/Home.vue'
import Connexion from './pages/Connexion.vue'
import Settings from './pages/Settings.vue'
import Template from './pages/template/Template.vue'
import ModuleMenu from './pages/ModuleMenu.vue'
// use
Vue.use(Vuex);
Vue.use(Router);
Vue.use(iView);
Vue.use(VueQuillEditor, /* { default global options } */)
// config
Vue.config.productionTip = false;
// store
const store = new Vuex.Store({
    state: storeService.load(),
    mutations: {
        "reset": storeService.mutation((state, params) => {
            state.store = storeService.defaultStore().store;
        }),
        "user": storeService.mutation((state, params) => {
            state.store.user = params
        }),
        "authenticated": storeService.mutation((state, params) => {
            state.store.authenticated = params
        })
    }
});
// routing
const router = new Router({
    routes: [
        {
            path: '/',
            name: "Dashboard",
            component: Home
        },
        {
            path: '/home',
            name: "Dashboard",
            component: Home
        },
        {
            path: '/module-page',
            name: "Page",
            component: ModulePage
        },
        {
            path: '/module-role',
            name: "Rôles",
            component: ModuleRole
        },
        {
            path: '/module-role/create',
            name: "Créer un rôle",
            component: RoleCreate
        },
        {
            path: '/module-role/:id',
            name: "Modifier un rôle",
            component: RoleUpdate
        },
        {
            path: '/module-menu',
            name: "Éditer le menu",
            component: ModuleMenu
        },
        {
            path: '/module-page/create',
            name: "Créer une page",
            component: PageCreate
        },
        {
            path: '/module-page/:id',
            name: "Modifier une page",
            component: PageUpdate
        },
        {
            path: '/template/:url_simple',
            name: "Template",
            component: Template
        },
        {
            path: '/module-user',
            name: "Gestion utilisateur",
            component: ModuleUser
        },
        {
            path: '/connection',
            name: "Connexion",
            component: Connexion
        },
        {
            path: '/settings',
            name: "Réglages",
            component: Settings
        }
    ]
});
router.beforeEach((to, from, next) => {
    let publicRoutes = [
        "/connection"
    ];
    // si on est connecté
    if (store.state.store.authenticated) {
        // si on est pas sur une route public alors qu'on est connecté on laisse passer les pages
        if (publicRoutes.indexOf(to.path) === -1) return next();
        // si on sur une route public alors qu'on est connecté on redirige vers l'accueil
        else return next("/");
    }
    // si on est pas connecté
    else {
        // si on est pas sur une route public
        if (publicRoutes.indexOf(to.path) === -1) return next("/connection");
        // si on est sur une route public alors qu'on est pas connecté on laisse passer les pages
        else return next();
    }
});
// register
Vue.component('v-page', Page);
Vue.component('v-header', Header);
Vue.component('v-image', Image);
Vue.component('v-menu', Menu);
Vue.component('v-breadcrum', Breadcrum);
Vue.component('v-table', Table);
Vue.component('v-line', Line);
Vue.component('v-column', Column);
Vue.component('v-table-head', TableHead);
Vue.component('v-table-body', TableBody);
Vue.component('v-searchbar', SearchBar);
// instance
new Vue({
    el: '#App',
    router,
    store,
    template: '<App/>',
    components: {App}
});
