import Api from "../Api";

class MenuApi extends Api {
  select(type = {}) {
    let formData = new FormData();
    for (let name in type) {
      formData.append(name, type[name]);
    }
    return this.post({route: "menu/index", formData});
  }

  update(type) {
    let formData = new FormData();
    for (let name in type) {
      formData.append(name, type[name]);
    }
    return this.post({route: "menu/update", formData});
  }
}

export default MenuApi;
