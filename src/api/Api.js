import App from "../App";
import apiService from "../services/api/ApiService";

class Api extends App {
    post(v){
        return apiService.post(v);
    }
}

export default Api;