import Api from "../Api";

class RoleApi extends Api {
    select(role = {}){
        let formData = new FormData();
        for(let name in role){
            formData.append(name, role[name]);
        }
        return this.post({route:"role/index",formData});
    }

    insert(role){
        let formData = new FormData();
        for(let name in role){
            formData.append(name, role[name]);
        }
        return this.post({route:"acl/addAclRole", formData});
    }

    delete(role){
        let formData = new FormData();
        for(let name in role){
            formData.append(name, role[name]);
        }
        return this.post({route:"role/delete", formData});
    }

    update(role){
        let formData = new FormData();
        for(let name in role){
            formData.append(name, role[name]);
        }
        return this.post({route:"role/update", formData});
    }
}

export default RoleApi;