import Api from "../Api";

class AclApi extends Api {
    select(acl = {}){
        return this.post({route:"acl/index",acl});
    }

    insert(acl){
        let formData = new FormData();
        for(let name in acl){
            formData.append(name, acl[name]);
        }
        return this.post({route:"acl/add", formData});
    }

    delete(acl){
        let formData = new FormData();
        for(let name in acl){
            formData.append(name, acl[name]);
        }
        return this.post({route:"acl/delete", formData});
    }

    update(acl){
        let formData = new FormData();
        for(let name in acl){
            formData.append(name, acl[name]);
        }
        return this.post({route:"acl/update", formData});
    }
}

export default AclApi;
