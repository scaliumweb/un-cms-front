import Api from "../Api";

class UserApi extends Api {
    select(){
        return this.post({route:"user/index"});
    }

    auth(user){
        let formData = new FormData();
        for(let name in user){
            formData.append(name, user[name]);
        }
        return this.post({route:"user/login", formData});
    }

    disconnect(user){
        let formData = new FormData();
        for(let name in user){
            formData.append(name, user[name]);
        }
        return this.post({route:"user/logout", formData});
    }

    insert(user){
        let formData = new FormData();
        for(let name in user){
            formData.append(name, user[name]);
        }
        return this.post({route:"user/add", formData});
    }

    delete(user){
        let formData = new FormData();
        for(let name in user){
            formData.append(name, user[name]);
        }
        return this.post({route:"user/delete", formData});
    }

    update(user){
        let formData = new FormData();
        for(let name in user){
            formData.append(name, user[name]);
        }
        return this.post({route:"user/update", formData});
    }
}

export default UserApi;