import Api from "../Api";

class PageApi extends Api {
    select(page = {}){
      let formData = new FormData();
      for(let name in page){
          formData.append(name, page[name]);
      }
        return this.post({route:"page/index",formData});
    }

    insert(page){
        let formData = new FormData();
        for(let name in page){
            formData.append(name, page[name]);
        }
        return this.post({route:"page/add", formData});
    }


  pageByUrl(page){
        let formData = new FormData();
        for(let name in page){
            formData.append(name, page[name]);
        }
        return this.post({route:"page/pageByUrl", formData});
    }

    delete(page){
        let formData = new FormData();
        for(let name in page){
            formData.append(name, page[name]);
        }
        return this.post({route:"page/delete", formData});
    }

    update(page){
        let formData = new FormData();
        for(let name in page){
            formData.append(name, page[name]);
        }
        return this.post({route:"page/update", formData});
    }
}

export default PageApi;
