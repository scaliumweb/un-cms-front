import Service from "../Service";

class StoreService extends Service {
    constructor(){
        super();
    }

    save(state) {
        localStorage.setItem('store', JSON.stringify(state));
    }

    load() {
        let store = localStorage.getItem('store');
        return store ? JSON.parse(store) : this.defaultStore();
    }

    defaultStore() {
        return {
            store: {
                user: {},
                authenticated: false
            }
        };
    }

    mutation(callback) {
        return (state, pauload) => {
            callback(state, pauload);
            this.save(state);
        }
    }
}

export default new StoreService();