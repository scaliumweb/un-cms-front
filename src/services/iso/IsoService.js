import Service from "../Service";

class IsoService extends Service {
    /**
     * @param {Boolean} error
     * @param {String} errorMessage
     * @param {*} data
     */
    constructor({error, errorMessage, data}) {
        super();
        this.error = error !== undefined ? error : false;
        this.errorMessage = errorMessage !== undefined ? errorMessage : false;
        this.data = data !== undefined ? data : false;
    }
}

export default IsoService;
