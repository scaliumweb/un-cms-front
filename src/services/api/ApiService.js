import Service from "../Service";
import IsoService from "../iso/IsoService";

class ApiService extends Service {
    constructor() {
        super();
    }

    /**
     * @public
     * @param {String} route
     * @param {FormData} [formData]
     * @return {Promise}
     */
    post({route, formData}) {
        return this.http({
            route,
            type: "POST",
            formData
        })
    }

    /**
     * @private
     * @param {String} route
     * @param {String} type
     * @param {FormData} formData
     * @return {Promise}
     */
    http({route, type, formData = new FormData}) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            /*xhr.open(type, "/un-cms/api/ws/" + route + "/index.php");*/
            xhr.open(type, "/un-cms/api/ws/" + route + ".php");
            /*xhr.open(type, "http://51.38.179.198/un-cms/api/ws/" + route + ".php");*/
            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        try {
                            let response = xhr.responseText;
                            let data = JSON.parse(response);
                            return resolve(new IsoService({data: data.data}));
                        } catch (e) {
                            return reject(new IsoService({
                                errorMessage: "json badly formated",
                                error: true,
                                data: xhr.responseText0
                            }));
                        }
                    } else return reject(new IsoService({
                        errorMessage: "erreur serveur : " + xhr.status,
                        error: true,
                        data: xhr.responseText
                    }));
                }
            };
            xhr.send(formData);
        });
    }
}

export default new ApiService();