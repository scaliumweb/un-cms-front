import Type from "../Type";

class UserType extends Type {
    constructor(user = {}){
        super();
        // back
        if(user.id_user !== undefined) user.id = user.id_user;
        if(user.firstname !== undefined) user.firstName = user.firstname;
        if(user.lastname !== undefined) user.lastName = user.lastname;
        if(user.email !== undefined) user.email = user.email;
        if(user.is_active !== undefined) user.active = user.is_active;
        if(user.is_deleted !== undefined) user.deleted = user.is_deleted;
        if(user.gender !== undefined) user.gender = user.gender;
        if(user.password !== undefined) user.password = user.password;
        if(user.id_role !== undefined) user.role_id = user.id_role;
        if(user.token !== undefined) user.token = user.token;
        // front
        this.id = user.id !== undefined ? user.id : "";
        this.firstName = user.firstName !== undefined ? user.firstName : "";
        this.lastName = user.lastName !== undefined ? user.lastName : "";
        this.email = user.email !== undefined ? user.email : "";
        this.active = user.active !== undefined && user.active === "1";
        this.deleted = user.deleted !== undefined && user.deleted === "1";
        this.gender = user.gender !== undefined ? user.gender : "";
        this.password = user.password !== undefined ? user.password : "";
        this.role_id = user.role_id !== undefined ? user.role_id : "";
        this.token = user.token !== undefined ? user.token : "";
    }
    toBack(){
        let type = {};
        type.id_user = this.id;
        type.firstname = this.firstName;
        type.lastname = this.lastName;
        type.email = this.email;
        type.is_active = this.active ? "1" : 0;
        type.is_deleted = this.deleted ? "1" : 0;
        type.gender = this.gender;
        type.password = this.password;
        type.id_role = this.role_id;
        type.token = this.token;
        return type;
    }
}

export default UserType;