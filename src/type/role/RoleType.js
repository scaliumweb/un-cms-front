import Type from "../Type";

class RoleType extends Type {
    constructor(role = {}){
        super();
        // back
        if(role.id_role !== undefined) role.id = role.id_role;
        if(role.name !== undefined) role.name = role.name;
        // front
        this.id = role.id !== undefined ? role.id : "";
        this.name = role.name !== undefined ? role.name : "";
    }
    toBack(){
        let type = {};
        type.id_role = this.id;
        type.name = this.name;
        return type;
    }
}

export default RoleType;