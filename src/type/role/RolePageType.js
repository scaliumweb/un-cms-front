import Type from "../Type";

class RolePageType extends Type {
    constructor(rolepage = {}){
        super();
        // back
        if(rolepage.id_role_page !== undefined) rolepage.id = rolepage.id_role_page;
        if(rolepage.id_role !== undefined) rolepage.id_role = rolepage.id_role;
        if(rolepage.id_page !== undefined) rolepage.id_page = rolepage.id_page;
        // front
        this.id = rolepage.id !== undefined ? rolepage.id : "";
        this.id_role = rolepage.id_role !== undefined ? rolepage.id_role : "";
        this.id_page = rolepage.id_page !== undefined ? rolepage.id_page : "";
    }
    toBack(){
        let type = {};
        type.id_role_page = this.id;
        type.id_role = this.id_role;
        type.id_page = this.id_page;
        return type;
    }
}

export default RolePageType;