import App from "../App";

class Type extends App {
    constructor(){
        super();
    }

    collection(datas, type){
        let collections = [];
        if(!datas) return collections;
        for(let data of datas){
            collections.push(new type(data));
        }
        return collections;
    }
}

export default Type;