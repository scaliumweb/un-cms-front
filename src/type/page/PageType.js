import Type from "../Type";

class PageType extends Type {
    constructor(page = {}){
        super();
        // back
        if(page.id_page !== undefined) page.id = page.id_page;
        if(page.title !== undefined) page.title = page.title;
        if(page.content !== undefined) page.content = page.content;
        if(page.date_add !== undefined) page.add = page.date_add;
        if(page.date_update !== undefined) page.update = page.date_update;
        if(page.is_active !== undefined) page.active = page.is_active;
        if(page.url_simple !== undefined) page.urlsimple = page.url_simple;
        // front
        this.id = page.id !== undefined ? page.id : "";
        this.title = page.title !== undefined ? page.title : "";
        this.content = page.content !== undefined ? page.content : "";
        this.add = page.add !== undefined ? page.add : "";
        this.update = page.add !== undefined ? page.add : "";
        this.active = page.active !== undefined ? page.active === "1" : true;
        this.urlsimple = page.urlsimple !== undefined ? page.urlsimple : "";
    }
    toBack(){
        let type = {};
        type.id_page = this.id;
        type.title = this.title;
        type.content = this.content;
        type.date_add = this.add;
        type.date_update = this.update;
        type.is_active = this.active ? "1" : "0";
        type.url_simple = this.urlsimple;
        return type;
    }
}

export default PageType;